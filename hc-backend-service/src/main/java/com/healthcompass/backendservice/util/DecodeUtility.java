package com.healthcompass.backendservice.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAmount;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Columns;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthcompass.backendservice.controller.BackendNotificationController;
import com.healthcompass.backendservice.model.AddressType;
import com.healthcompass.backendservice.model.ContactPointType;
import com.healthcompass.backendservice.model.HumanNameType;
import com.macasaet.fernet.Key;
import com.macasaet.fernet.StringValidator;
import com.macasaet.fernet.Token;
import com.macasaet.fernet.Validator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Slf4j
public class DecodeUtility  {
  
	
	private String name;
	private String address;
	private String phone;
	private String dateOfBirth;
	private transient HumanNameType compositeName;
	private transient AddressType compositeAddress;
	private transient ContactPointType compositePhone;
	
	private static String encodeDecodeKey = "CSMMNtPnLo9aE64V6XLuOZO-bYNslmFO3oFXF6RA-K8=";
	    
	
	
	public DecodeUtility() {
		
	}
	
	public HumanNameType getCompositeName(String name) {
		
		
		if (this.compositeName == null){
			this.compositeName=new HumanNameType(name);
		}
		return this.compositeName;
	}
	
	public void setCompositeName(HumanNameType compositeName) {
		
		this.compositeName=compositeName;
	}
	
	public ContactPointType getCompositePhone(String phone) {
		
		if (this.compositePhone == null){
			this.compositePhone=new ContactPointType(phone);
		}
		return this.compositePhone;
	}
	
	public void setCompositePhone(ContactPointType compositePhone) {
		
		this.compositePhone=compositePhone;
	}
	
	public AddressType getCompositeAddress(String Address) {
		
		if (this.compositeAddress == null){
			this.compositeAddress=new AddressType(address);
		}
		return this.compositeAddress;
	}
	
	public void setCompositeAddress(AddressType compositeAddress) {
		
		this.compositeAddress=compositeAddress;
	}
	
	
	public String getDecodedName(String encodedName) {
	
	// return null;
			return  getDecodedValue("Name",encodedName);
	
	}
	public String getDecodedPhone(String encodedPhone) {
		
			//return getDecodedValue("Phone Number",this.getCompositePhone(encodedPhone).getValue());
		return getDecodedValue("Phone Number",encodedPhone);
		
		 
	}
	
	public String getDecodedAddress(String address) {
		
			return getDecodedValue("Address Text",this.getCompositeAddress(address).getText()) +" ";
			
		
		
		
	   		
	}
	public String getDecodedDateOfBirth(String dateOfBirth) {
		
		return getDecodedValue("Date of Birth",dateOfBirth);
	}
	
	public String getDecodedEmail(String encodedEmail) {
		
		return getDecodedValue("Email",encodedEmail);
	}
	
	/*public String getDecodedEmail(String compositeName) {
		String name = "";
		String[] multipleStrings = compositeName.split(",");
		 name = multipleStrings[2];
		 return name;
	}*/
	
	public int  getAge(String dob) {
		int age=0;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			Instant instant = sdf.parse(dob).toInstant();
		    ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
		    LocalDate dobDate = zone.toLocalDate();
		      //Calculating the difference between given date to current date.
		    Period period = Period.between(dobDate, LocalDate.now());
		    age=period.getYears();
		}catch(Exception e) {
			
		}
		
		return age;
	}
	
	public static String getDecodedValue(String attributeName,String encodedValue) {
		
		
		try {
			final Key key = new Key(encodeDecodeKey);
			if(encodedValue == null || encodedValue.isEmpty()) {
				log.warn("Encoded Value of "+attributeName+" is null or empty ! Cannot decode. returning the encoded value as is");
				return null;
			}
			//String encodedPatientName = getName("{\"(154,gAAAAABgacGWoJVG18JfA7lkQzUXzRbgMog1y1T2VgJg01S8Kfxv9ZnldSg4ADlMjI_RcKLrm0NOS4txNjsH3kxvpano5r4mBQ==,gAAAAABgacGWM4DRCeHKqO0Udc_az_ZSXOBagAPhLYTq7tAwTaT9bmHtcS2fwy-emnnelMgWSCY9OYctJAXclHwdynloORG3HQ==,gAAAAABgacGWRy3xbBtmh0uzGKTs7qUtxF7vYZZ6bActI3XL3Q2VeOhmWmTgForZyjL3BBPZAPk4Ujw-mr1mOBCU0ZWgOP8Oaw==,,,\\\"(\\\"\\\"2021-04-04 19:09:30\\\"\\\",\\\"\\\"9999-12-31 05:30:00\\\"\\\")\\\")\"}");
			final Token token = Token.fromString(encodedValue);
			
	       
	        final Validator<String> validator = new StringValidator() {
	    		public TemporalAmount getTimeToLive() {
	                return Duration.ofSeconds(Instant.MAX.getEpochSecond());
	            }
	    		};
	        final String payload = token.validateAndDecrypt(key, validator);
	        log.debug("Attribute Name:"+attributeName+"");
	        
	        log.debug("Payload is " + payload);
	        return payload;
		}catch(Exception e) {
			log.error("Error while decoding Attribute:"+attributeName+" : Message:"+e.getMessage());
			log.debug("Encoded Value:"+encodedValue+"");
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	
}
