package com.healthcompass.backendservice.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.backendservice.controller.BackendNotificationController;
import com.healthcompass.backendservice.model.MessageRecepient;
import com.healthcompass.backendservice.util.DecodeUtility;

import lombok.extern.slf4j.Slf4j;



@Component
@Slf4j
public class IncompleteProfileForPatientsResultSetExtractor implements ResultSetExtractor{
	
	
	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    
	    	List<MessageRecepient> messageRecepients = new ArrayList<MessageRecepient>();
	        
	         while (rs.next()) {
	        	 MessageRecepient recepient = new MessageRecepient();
	        	 recepient.setUserName(rs.getString("userName"));
	        	
	        	
	        	 DecodeUtility decodeUtil = new DecodeUtility();
	        	 recepient.setFirstName(decodeUtil.getDecodedName(rs.getString("firstName")));
	        	 recepient.setLastName(decodeUtil.getDecodedName(rs.getString("lastName")));
	        	 recepient.setEmail(decodeUtil.getDecodedEmail(rs.getString("email"))); // this will be encrypted actually.
	        	 recepient.setPhone(decodeUtil.getDecodedPhone(rs.getString("phoneNumber")));
	        	 
	        	 recepient.setRecepientType(1);
	        	 messageRecepients.add(recepient);
	        	 
	        	
	         
	        }
	         
	        return  messageRecepients ;
	    }
	
}
