package com.healthcompass.backendservice.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.backendservice.controller.BackendNotificationController;
import com.healthcompass.backendservice.model.PractitionerInfo;
import com.healthcompass.backendservice.util.DecodeUtility;

import lombok.extern.slf4j.Slf4j;



@Component
@Slf4j
public class PractitionersInfoResultSetExtractor implements ResultSetExtractor{
	
	
	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    
	    	Map<UUID,PractitionerInfo> practitionerInfos = new HashMap<UUID,PractitionerInfo>();
	    	
	        
	         while (rs.next()) {
	        	 PractitionerInfo practitionerInfo = new PractitionerInfo();
	        	 practitionerInfo.setPractitionerId((UUID) rs.getObject("practitionerId"));
	        	 practitionerInfo.setPractitionerUserName(rs.getString("userName"));
	        	
	        	
	        	 DecodeUtility decodeUtil = new DecodeUtility();
	        	 practitionerInfo.setPractitionerFirstName(decodeUtil.getDecodedName(rs.getString("firstName")));
	        	 practitionerInfo.setPractitionerLastName(decodeUtil.getDecodedName(rs.getString("lastName")));
	        	 practitionerInfo.setPractitionerPhone(decodeUtil.getDecodedPhone(rs.getString("phoneNumber")));
	        	 practitionerInfo.setPractitionerEmail(decodeUtil.getDecodedEmail(rs.getString("email"))); 
	        	 
	        	 
	        	 
	        	 practitionerInfos.put(practitionerInfo.getPractitionerId(),practitionerInfo);
	        	
	        	 
	        	
	         
	        }
	         
	        return  practitionerInfos ;
	    }
	
}
