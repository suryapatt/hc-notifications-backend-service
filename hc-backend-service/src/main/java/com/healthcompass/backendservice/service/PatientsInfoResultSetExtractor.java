package com.healthcompass.backendservice.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.backendservice.controller.BackendNotificationController;
import com.healthcompass.backendservice.model.PatientInfo;
import com.healthcompass.backendservice.util.DecodeUtility;

import lombok.extern.slf4j.Slf4j;



@Component
@Slf4j
public class PatientsInfoResultSetExtractor implements ResultSetExtractor{
	
	
	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    
	    	Map<UUID,PatientInfo> patientInfos = new HashMap<UUID,PatientInfo>();
	    	
	        
	         while (rs.next()) {
	        	 PatientInfo patientInfo = new PatientInfo();
	        	 patientInfo.setPatientId((UUID) rs.getObject("patientId"));
	        	 patientInfo.setPatientUserName(rs.getString("userName"));
	        	
	        	
	        	 DecodeUtility decodeUtil = new DecodeUtility();
	        	 patientInfo.setPatientFirstName(decodeUtil.getDecodedName(rs.getString("firstName")));
	        	 patientInfo.setPatientLastName(decodeUtil.getDecodedName(rs.getString("lastName")));
	        	 patientInfo.setPatientPhone(decodeUtil.getDecodedPhone(rs.getString("phoneNumber")));
	        	 patientInfo.setPatientEmail(decodeUtil.getDecodedEmail(rs.getString("email"))); 
	        	 
	        	 patientInfos.put(patientInfo.getPatientId(),patientInfo);
	        	
	        	 
	        	
	         
	        }
	         
	        return  patientInfos ;
	    }
	
}
