package com.healthcompass.backendservice.service;

import java.util.List;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.Timestamp;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.QueryResults;
import com.google.cloud.datastore.StringValue;
import com.google.cloud.datastore.StructuredQuery.CompositeFilter;
import com.google.cloud.datastore.StructuredQuery.PropertyFilter;
import com.healthcompass.backendservice.controller.BackendNotificationController;
import com.healthcompass.backendservice.model.AppointmentInfo;
import com.healthcompass.backendservice.model.MessageRecepient;
import com.healthcompass.backendservice.model.PatientInfo;
import com.healthcompass.backendservice.model.PractitionerInfo;
import com.healthcompass.backendservice.model.ServiceRequestInfo;
import com.healthcompass.backendservice.request.NotificationRequest;
import com.healthcompass.backendservice.util.DateTimeUtility;
import com.healthcompass.backendservice.util.GoogleJWTokenGenerator;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;



@Component
@Slf4j
public class NotificationsSchedulerService {
	
	@Autowired 
	FirestoreService firestoreService;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("${hc-notification-service-url}")
	private String healthCompassNotificationServiceURL;
	
	@Value("${health-compass-web-url}")
	private String healthCompassWebURL;
	
	@Value("${backend-service-timezone}")
	private String backendServiceTimeZone;
	
	@Value("${backend-service-language}")
	private String backendServiceLanguage;
	
	 @Value("${backend-service-default-date-format}")
	 private String backendServiceDefaultDateFormat;
	
	
	 @Value("${backend-service-default-only-date-format}")
	 private String backendServiceDefaultOnlyDateFormat;
	 
	 @Value("${backend-service-delete_messages-days}")
	 private String backendServiceDefaultMessageDeleteDays;
	 
	@Autowired
	IncompleteProfileForPatientsResultSetExtractor incompleteProfileForPatientsResultSetExtractor;
	
	@Autowired
	IncompleteProfileForPractitionerResultSetExtractor incompleteProfileForPractitionerResultSetExtractor;
	
	@Autowired
	IncompleteProfileForProviderResultSetExtractor incompleteProfileForProviderrResultSetExtractor;
	
	@Autowired
	AppointmentInofResultSetExtractor appointmentInofResultSetExtractor;
	
	@Autowired
	PatientsInfoResultSetExtractor patientsInfoResultSetExtractor;
	
	@Autowired
	PractitionersInfoResultSetExtractor practitionersInfoResultSetExtractor;
	
	@Autowired
	GoogleJWTokenGenerator googleJWTokenGenerator;
	
	//@Scheduled(cron="0 0 * ? * *")
	public void scheduleNotifications() {
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	      Date now = new Date();
	      String strDate = sdf.format(now);
	      System.out.println("Scheduled Notifications:: " + strDate);
		
	}
	
	//@Scheduled(cron="*/3 * * * * *")
	public void scheduleEveryThreeMin() {
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	      Date now = new Date();
	      String strDate = sdf.format(now);
	      System.out.println("second scheduler :: " + strDate);
	      
	      DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
			String timeInAMPM = dateFormat.format(new Date());
			String timeInhr = timeInAMPM.substring(0, 2);
			String ampm = timeInAMPM.substring(6);
			String exactHourTime = timeInhr+":00 "+ampm;
			System.out.println(exactHourTime);
			processAllMessagesDueThisHour(exactHourTime);
			
		
	}
	
	public void processAllMessagesDueThisHour(String exactHourTime) {
		
	    log.info("Identifying all jobs that are due at "+exactHourTime+ " in "+backendServiceTimeZone+" Timezone");
	    
		Map<String,String> methodMap = firestoreService.getAllMessagesDueThisHour(exactHourTime);
		if(!methodMap.isEmpty()) {
			log.info("Following jobs will be executed now at:"+exactHourTime+" in "+backendServiceTimeZone+" Timezone");
			for(String messageTriggerId : methodMap.keySet()) {
				String methodName = methodMap.get(messageTriggerId);
				log.info(""+methodName+"\r\n");
			}
		}else{
			log.info("No jobs are scheduled now at "+exactHourTime+ " in "+backendServiceTimeZone+" Timezone");
			log.info("Exiting!");
		}
		for(String messageTriggerId : methodMap.keySet()) {
			String methodName = methodMap.get(messageTriggerId);
			Method method = null;
			try {
				  method = this.getClass().getMethod(methodName, String.class);
				
				}  catch (NoSuchMethodException e) { e.printStackTrace(); }
			
			
				if(method != null) {
					try {
					 method.invoke(this, messageTriggerId);
				
				 
				} catch (IllegalArgumentException e) { e.printStackTrace(); }
				  catch (IllegalAccessException e) { e.printStackTrace(); }
				  catch (InvocationTargetException e) { e.printStackTrace(); }
				}
	    }
		
	}
	
	public void  processIncompleteBasicProfiles(String messageTriggerId){
		
		
		String inCompleteBasicProfilesPatientQuery = " select usertab.username as userName, "
													+ " (pat.name)[1].given as firstName, "
													+ " (pat.name)[1].family as lastName, "
													+ " (pat.telecom)[2].value as email, "
													+ " (pat.telecom)[1].value as phoneNumber, "
													+ " pat.birth_date, "
													+ " pat.gender, "
													+ " pat.marital_status, "
													+ " pat.blood_group "
													+ " from project_asclepius.patient pat, "
													+ " project_asclepius.auth_user usertab "
													+ " where pat.userid=usertab.id "
													+ " and ((pat.telecom)[2].value is null "
													+ " or (pat.telecom)[1].value is null "
													+ " or  (pat.name)[1].given is null "
													+ " or (pat.name)[1].family is null "
													+ " or pat.birth_date is null "
													+ " or pat.gender is null "
													+ " or pat.marital_status is null"
													+ " or pat.blood_group is null ) "; 
		log.debug("Executing patients incomplete basic profiles Query: "+inCompleteBasicProfilesPatientQuery+"");
		ArrayList<MessageRecepient> recepientsList = (ArrayList<MessageRecepient>) jdbcTemplate.query(inCompleteBasicProfilesPatientQuery,incompleteProfileForPatientsResultSetExtractor);
		if(recepientsList == null || recepientsList.isEmpty()) {
			log.info("No Patients are found with incomplete basic profiles");
		}else {
			log.info("Retrieved "+recepientsList.size()+" Patients with incomplete basic profiles");
		}
		String inCompleteBasicProfilesPractitionerQuery = " select usertab.username as userName,"
				+ " (pract.name)[1].given as firstName,"
				+ " (pract.name)[1].family as lastName,"
				+ " (pract.telecom)[2].value as email,"
				+ " (pract.telecom)[1].value as phoneNumber,"
				+ " pract.birth_date,pract.gender" 
				+ "  from project_asclepius.practitioner pract, "
				+ " project_asclepius.auth_user usertab where "
				+ " pract.userid=usertab.id"
				+ " and ((pract.telecom)[2].value is null "
				+ "	 or (pract.telecom)[1].value is null " 
				+ "	 or  (pract.name)[1].given is null " 
				+ " or (pract.name)[1].family is null " 
				+ "	 or pract.birth_date is null "
				+ "	 or pract.gender is null) "; 

		log.debug("Executing Practitioners incomplete basic profiles Query: "+inCompleteBasicProfilesPractitionerQuery+"");
		ArrayList<MessageRecepient> practitionerRecepientsList = (ArrayList<MessageRecepient>) jdbcTemplate.query(inCompleteBasicProfilesPractitionerQuery,incompleteProfileForPractitionerResultSetExtractor);
		if(practitionerRecepientsList == null || practitionerRecepientsList.isEmpty()) {
			log.info("No Practitioners found with incomplete basic profiles");
		}else {
			log.info("Retrieved "+practitionerRecepientsList.size()+" Practitioners with incomplete basic profiles");
		}
		if(recepientsList == null) {
			recepientsList = new ArrayList<MessageRecepient>();
		}
		recepientsList.addAll(practitionerRecepientsList);

		String inCompleteBasicProfilesProviderQuery = " select usertab.username as userName,"
				+ " org.name as firstName,"
				+ " (org.telecom)[2].value as email," 
				+ " (org.telecom)[1].value as phoneNumber " 
				+ " from project_asclepius.organization org, "
				+ " project_asclepius.auth_user usertab where " 
				+ " org.userid=usertab.id " 
				+ " and (org.telecom)[2].value is null "
				+ " or org.name is null " 
				+ "	 or (org.telecom)[1].value is null " 
				+ "	 or org.type is null " 
				+ " or org.part_of is null"; 

		log.debug("Executing Providers incomplete basic profiles Query: "+inCompleteBasicProfilesProviderQuery+"");
		ArrayList<MessageRecepient> providerRecepientsList = (ArrayList<MessageRecepient>) jdbcTemplate.query(inCompleteBasicProfilesProviderQuery,incompleteProfileForProviderrResultSetExtractor);
		
		if(providerRecepientsList == null || providerRecepientsList.isEmpty()) {
			log.info("No Providers found with incomplete basic profiles");
		}else {
			log.info("Retrieved "+providerRecepientsList.size()+" Providers with incomplete basic profiles");
		}
		if(recepientsList == null) {
			recepientsList = new ArrayList<MessageRecepient>();
		}
		
		recepientsList.addAll(providerRecepientsList); 
		log.info("Calling the Notification Service for "+recepientsList.size()+" Recepients one by one for incomplete basic profiles");
		Date startTime = new Date();
		for(MessageRecepient recepient : recepientsList) {
			
			 NotificationRequest sendNotificationRequest = new  NotificationRequest();
			 sendNotificationRequest.setMessageTriggerId(Integer.parseInt(messageTriggerId));
			 HashMap messageKeys = new HashMap<String,String>();
			 messageKeys.put("user_name", recepient.getUserName());
			 messageKeys.put("user_first_name", recepient.getFirstName());
			 messageKeys.put("user_email", recepient.getEmail());
			 messageKeys.put("user_phone_number", recepient.getPhone());
			 messageKeys.put("unique_identifier", "BasicProfileUpdate-"+recepient.getUserName());
			 messageKeys.put("web_portal", healthCompassWebURL);
			 sendNotificationRequest.setMessageKeys(messageKeys);
			 sendToNotificationService(sendNotificationRequest);
			 
		  } // end for each recepient
		log.info("Time taken to send incomplete basic profiles\r\n"
				+ " notifications for "+recepientsList.size()+" Recepients is"+DateTimeUtility.calculateTimeDuration(startTime)+" seconds");
		
		
	}
	
   public String  sendToNotificationService(NotificationRequest sendNotificationRequest){
	   
	   String sendNotificationResponseAsJsonStr = null;
		try {
			restTemplate = new RestTemplate();
		    HttpHeaders sendNotificationRequestHeaders = new HttpHeaders();
		    sendNotificationRequestHeaders.setContentType(MediaType.APPLICATION_JSON);
		    sendNotificationRequestHeaders.add("user-lang", backendServiceLanguage);
		    googleJWTokenGenerator.checkAndAddAuthorizationHeader(sendNotificationRequestHeaders);
		    HttpEntity<NotificationRequest> httpRequest = 
		    	      new HttpEntity<NotificationRequest>(sendNotificationRequest, sendNotificationRequestHeaders);
		    ObjectMapper objectMapper = new ObjectMapper();	
		    log.debug("Calling Notification Service with request:"+sendNotificationRequest.toString()+"");
		    sendNotificationResponseAsJsonStr = 
		    	      restTemplate.postForObject(healthCompassNotificationServiceURL, httpRequest, String.class);
		    	    
		    
		}catch(Exception e) {
			log.error("Error from Notifications Service for the the request:"+sendNotificationRequest+"");
			log.error("Error Message is:"+e.getMessage()+"");
			// Logger Service
		}
		return sendNotificationResponseAsJsonStr;
	  
 } // end 
	
   
	public  void processIncompleteProfiles(String messageTriggerId){
		
		
		String inCompleteProfilesPatientQuery = " select usertab.username as userName,"
				+ " (pat.name)[1].given as firstName,"
				+ " (pat.name)[1].family as lastName, "
				+ " (pat.telecom)[2].value as email,"
				+ " (pat.telecom)[1].value as phoneNumber "
				+ " from project_asclepius.patient pat, " 
				+ " project_asclepius.auth_user usertab "
				+ " where pat.userid=usertab.id " 
				+ " and ((pat.telecom)[2].value is not null " 
				+ "	 and (pat.telecom)[1].value is not null "
				+ "	 and  (pat.name)[1].given is not null "
			    + "	 and (pat.name)[1].family is not null "
				+ " and pat.birth_date is not null "
				+ " and pat.gender is not null " 
				+ "	 and pat.marital_status is not null " 
				+ " and pat.blood_group is not null) " 
				+ " and ((pat.address)[1].text is null " 
				+ " OR (pat.address)[1].line is null " 
				+ " OR (pat.address)[1].country is null " 
				+ " OR (pat.address)[1].state is null " 
				+ " OR (pat.address)[1].city is null " 
				+ " OR (pat.address)[1].postal_code is null " 
				+ " OR ((pat.contact)[1].name).given is null " 
				+ " OR ((pat.contact)[1].name).family is null " 
				+ " OR ((pat.contact)[1].telecom).value is null " 
				+ " OR ((pat.contact)[1].address).text is null " 
				+ " OR ((pat.contact)[1].address).line is null " 
				+ " OR ((pat.contact)[1].address).country is null " 
				+ " OR  ((pat.contact)[1].address).city is null " 
				+ " OR  ((pat.contact)[1].address).state is null " 
				+ " OR  ((pat.contact)[1].address).postal_code is null " 
				+ " OR  (pat.identifier)[1].type is null " 
				+ " OR  (pat.identifier)[1].value is null " 
				+ " OR  (pat.identifier)[1].assigner is null "
				+ "     )"; 
		
		log.debug("Executing patients incomplete profiles Query: "+inCompleteProfilesPatientQuery+"");
		
		ArrayList<MessageRecepient> recepientsList = (ArrayList<MessageRecepient>) jdbcTemplate.query(inCompleteProfilesPatientQuery,incompleteProfileForPatientsResultSetExtractor);
		
		if(recepientsList == null || recepientsList.isEmpty()) {
			log.info("No Patients are found with incomplete profiles");
		}else {
			log.info("Retrieved "+recepientsList.size()+" Patients with incomplete profiles");
		}
		
		String inCompleteProfilesPractitionerQuery = " select usertab.username as userName,"
				+ " (pract.name)[1].given as firstName,(pract.name)[1].family as lastName, "
				+ " (pract.telecom)[2].value as email,"
				+ " (pract.telecom)[1].value as phoneNumber " 
				+ " from project_asclepius.practitioner pract, "
				+ " project_asclepius.auth_user usertab where " 
				+ " pract.userid=usertab.id " 
				+ " and ((pract.telecom)[2].value is not null " 
				+ " and (pract.telecom)[1].value is not null " 
				+ " and  (pract.name)[1].given is not null " 
				+ " and (pract.name)[1].family is not null " 
				+ " and pract.birth_date is not null " 
				+ " and pract.gender is not null) " 
				+ " and ((pract.address)[1].text is null " 
				+ " OR (pract.address)[1].line is null " 
				+ " OR (pract.address)[1].country is null " 
				+ " OR (pract.address)[1].state is null " 
				+ " OR (pract.address)[1].city is null " 
				+ " OR (pract.address)[1].postal_code is null " 
				+ " OR (pract.qualification)[1].specialty is null " 
				+ " OR  (pract.identifier)[1].type is null " 
				+ " OR  (pract.identifier)[1].value is null " 
				+ " OR  (pract.identifier)[1].assigner is null " 
				+ "     ) "; 

		log.debug("Executing practitioners incomplete profiles Query: "+inCompleteProfilesPractitionerQuery+"");
		
		ArrayList<MessageRecepient> practitionerRecepientsList = (ArrayList<MessageRecepient>) jdbcTemplate.query(inCompleteProfilesPractitionerQuery,incompleteProfileForPractitionerResultSetExtractor);
		
		if(practitionerRecepientsList == null || practitionerRecepientsList.isEmpty()) {
			log.info("No Practitioners found with incomplete profiles");
		}else {
			log.info("Retrieved "+practitionerRecepientsList.size()+" Practitioners with incomplete profiles");
		}
		if(recepientsList == null) {
			recepientsList = new ArrayList<MessageRecepient>();
		}
		recepientsList.addAll(practitionerRecepientsList);
		
		

		String inCompleteProfilesProviderQuery = " select usertab.username as userName,"
				+ " org.name as firstName,"
				+ " usertab.email as email," 
				+ " (org.telecom)[2].value as email," 
				+ " (org.telecom)[1].value as phoneNumber "  
				+ " from project_asclepius.organization org, "
				+ " project_asclepius.auth_user usertab where " 
				+ " org.userid=usertab.id "
				+ " and (org.telecom)[2].value is not null "
				+ " and org.name is not null  " 
				+ " and (org.telecom)[1].value is not null " 
				+ " and org.type is not null "
				+ " and org.part_of is not null"
				+ " )"
				+ " and "
				+ " ( (org.address)[1].text is null"
				+ "    OR (org.address)[1].line is null"
				+ "    OR (org.address)[1].country is null"
				+ "    OR (org.address)[1].state is null"
				+ "    OR (org.address)[1].city is null"
				+ "    OR (org.address)[1].postal_code is null"
				+ "    OR  (org.identifier)[1].type is null"
				+ "    OR  (org.identifier)[1].value is null"
				+ "    OR  (org.identifier)[1].assigner is null"
				+ "    OR ((org.contact)[1].name).given is null"
				+ "    OR (org.contact)[1].purpose is null"
				+ "    OR ((org.contact)[1].name).family is null"
				+ "    OR ((org.contact)[1].telecom).value is null"
				+ "    OR ((org.contact)[1].address).text is null"
				+ "    OR ((org.contact)[1].address).line is null"
				+ "    OR ((org.contact)[1].address).country is null"
				+ "    OR  ((org.contact)[1].address).city is null"
				+ "    OR  ((org.contact)[1].address).state is null"
				+ "    OR  ((org.contact)[1].address).postal_code is null"
				+ "     )";
				
		log.debug("Executing Providers incomplete profiles Query: "+inCompleteProfilesProviderQuery+"");
		ArrayList<MessageRecepient> providerRecepientsList = (ArrayList<MessageRecepient>) jdbcTemplate.query(inCompleteProfilesProviderQuery,incompleteProfileForProviderrResultSetExtractor);
		if(providerRecepientsList == null || providerRecepientsList.isEmpty()) {
			log.info("No Providers found with incomplete profiles");
		}else {
			log.info("Retrieved "+providerRecepientsList.size()+" Providers with incomplete profiles");
		}
		if(recepientsList == null) {
			recepientsList = new ArrayList<MessageRecepient>();
		}
		recepientsList.addAll(providerRecepientsList); 
		log.info("Calling the Notification Service for "+recepientsList.size()+" Recepients one by one for incomplete profiles");
		Date startTime = new Date();
		for(MessageRecepient recepient : recepientsList) {
			
			 NotificationRequest sendNotificationRequest = new  NotificationRequest();
			 sendNotificationRequest.setMessageTriggerId(Integer.parseInt(messageTriggerId));
			 HashMap messageKeys = new HashMap<String,String>();
			 messageKeys.put("user_name", recepient.getUserName());
			 messageKeys.put("user_first_name", recepient.getFirstName());
			 messageKeys.put("user_email", recepient.getEmail());
			 messageKeys.put("user_phone_number", recepient.getPhone());
			 messageKeys.put("unique_identifier", "ProfileUpdate-"+recepient.getUserName());
			 messageKeys.put("web_portal", healthCompassWebURL);
			 sendNotificationRequest.setMessageKeys(messageKeys);
			 sendToNotificationService(sendNotificationRequest);
			 
		  } // end for each recepient
			
		log.info("Time taken to send incomplete profiles\r\n"
				+ " notifications for "+recepientsList.size()+" Recepients is"+DateTimeUtility.calculateTimeDuration(startTime)+" seconds");
		
	}
	
	public void processAppointmentFirstReminder(String messageTriggerId){
		
		
		String currentDayAppointmentsQuery = " select apt.id as appointmentId,apt.start as appointmentStart,"
				+ " apt.end as appointmentEnd ,"
				+ " apt.description as reasonToVisit ,"
				+ " ((apt.participant)[1].actor).reference as patient, " 
				+ "	((apt.participant)[3].actor).reference  as practitioner , "
				+ " ((apt.participant)[4].actor).display  as provider ," 
				+ "	((apt.participant)[5].actor).display  as location " 
				+ "	from project_asclepius.appointment apt " 
				+ "	WHERE apt.start >= CURRENT_DATE" 
				+ "	AND apt.start < (CURRENT_DATE + '1 day'::interval)"; 
		
		log.debug("Executing Appointment first reminder Query: "+currentDayAppointmentsQuery+"");
		
		ArrayList<AppointmentInfo> appointmentsList = (ArrayList<AppointmentInfo>) jdbcTemplate.query(currentDayAppointmentsQuery,appointmentInofResultSetExtractor);
		Set<UUID> patientIds = new HashSet<UUID>();
		Set<UUID> practitionerIds  = new HashSet<UUID>();
		Date startTime = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(backendServiceDefaultDateFormat);
    	//we will use default backend timezone to check for appointments
    	Date currentDate = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone)).getTime();
	    String currentDateAsString = sdf.format(currentDate);
	    
	    SimpleDateFormat sdfOnlyDateWithOutTime = new SimpleDateFormat(backendServiceDefaultOnlyDateFormat);
    	//we will use default backend timezone to check for appointments
    	Date currentDateWithOutTime = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone)).getTime();
	    String currentDateWithOutTimeAsString = sdfOnlyDateWithOutTime.format(currentDateWithOutTime);
	    
		if(appointmentsList == null || appointmentsList.isEmpty()) {
			log.info("No Appointments found for the date:" +currentDateWithOutTimeAsString+"\r\n");
			log.info("Exiting the method processAppointmentFirstReminder()");
			return;
		}else {
			log.info("Retrieved "+appointmentsList.size()+" Appointments for the date:"+currentDateWithOutTimeAsString+"");
		}
		for(AppointmentInfo appointment : appointmentsList) {
			patientIds.add(appointment.getPatientId())	;
			practitionerIds.add(appointment.getPractitionerId());
		}
		if(appointmentsList.size() > 0) {
			int i=1;
			String patientIdsInClause="";
			String patientInfoQuery = " select pat.id as patientId,usertab.username as userName, "
					+ " (pat.name)[1].given as firstName, "
					+ " (pat.name)[1].family as lastName, "
					+ " (pat.telecom)[2].value as email, "
					+ " (pat.telecom)[1].value as phoneNumber "
					+ "  from project_asclepius.patient pat, "
					+ " project_asclepius.auth_user usertab "
					+ " where pat.userid=usertab.id "
					+ "	 and pat.id in ("; 
			
		    		for(UUID id : patientIds) {
		    			
		    			patientIdsInClause+="'"+id+"'";
		    			
		    			if(i<patientIds.size()) {
		    				patientIdsInClause+=",";
		    				
		    			}
		    			i++;
		    			
		    			
		    		}
		    		patientInfoQuery+=patientIdsInClause+")";
		    		log.debug("Executing query to retrieve Patients information for Appointments: "+patientInfoQuery+"");
		    		HashMap<UUID,PatientInfo> patientInfos = (HashMap<UUID,PatientInfo>) jdbcTemplate.query(patientInfoQuery,patientsInfoResultSetExtractor);
		    		if(patientInfos == null || patientInfos.isEmpty()) {
		    			log.info("Not able to retrieve Patients with Appointments");
		    			log.info("Exiting the method processAppointmentFirstReminder()");
		    		}else {
		    			log.info("Retrieved "+patientInfos.size()+" Patients with Appointments for the date:"+currentDateWithOutTimeAsString+"");
		    		}
		    		
		    		i=1;
		    		String practitionerIdsInClause="";
		    		String practitionerInfoQuery = " select pract.id as practitionerId,usertab.username as userName, "
		    				+ " (pract.name)[1].given as firstName, "
		    				+ " (pract.name)[1].family as lastName, "
		    				+ " (pract.telecom)[2].value as email, "
		    				+ " (pract.telecom)[1].value as phoneNumber "
		    				+ "  from project_asclepius.practitioner pract, "
		    				+ " project_asclepius.auth_user usertab "
		    				+ " where pract.userid=usertab.id "
		    				+ "	 and pract.id in ("; 
		    		
		    	    		for(UUID id : practitionerIds) {
		    	    			
		    	    			practitionerIdsInClause+="'"+id+"'";
		    	    			
		    	    			if(i<practitionerIds.size()) {
		    	    				practitionerIdsInClause+=",";
		    	    				
		    	    			}
		    	    			i++;
		    	    			
		    	    			
		    	    		}
		    	    		practitionerInfoQuery+=practitionerIdsInClause+")";
		    	    		
		    	    		
		    	    		log.debug("Executing query to retrieve Practitioners information for Appointments: "+practitionerInfoQuery+"");
		    	    		HashMap<UUID,PractitionerInfo> practitionerInfos = (HashMap<UUID,PractitionerInfo>) jdbcTemplate.query(practitionerInfoQuery,practitionersInfoResultSetExtractor);
		    	    		
		    	    		if(practitionerInfos == null || practitionerInfos.isEmpty()) {
				    			log.info("Not able to retrieve Practitioners with Appointments");
				    			log.info("Exiting the method processAppointmentFirstReminder()");
				    		}else {
				    			log.info("Retrieved "+practitionerInfos.size()+" Practitioners with Appointments for the date:"+currentDateWithOutTimeAsString+"");
				    		}
		    	    		
		    	    		log.info("Calling the Notification Service for "+appointmentsList.size()+" Appointments one by one");
		    	    		Date start = new Date();
		    	    		for(AppointmentInfo appointment : appointmentsList) {
		    	    			
		    	    			
		    	   			 NotificationRequest sendNotificationRequest = new  NotificationRequest();
		    	   			 sendNotificationRequest.setMessageTriggerId(Integer.parseInt(messageTriggerId));
		    	   			 HashMap messageKeys = new HashMap<String,String>();
		    	   			 PatientInfo patient = patientInfos.get(appointment.getPatientId());
		    	   			 PractitionerInfo practitioner = practitionerInfos.get(appointment.getPractitionerId());
		    	   			 
		    	   			 messageKeys.put("patient_first_name", patient.getPatientFirstName());
		    	   			 messageKeys.put("patient_name", patient.getPatientFirstName()+" "+patient.getPatientLastName());
		    	   			 messageKeys.put("practitioner_first_name", practitioner.getPractitionerFirstName());
		    	   			 messageKeys.put("practitioner_name", practitioner.getPractitionerFirstName()+" "+practitioner.getPractitionerLastName());
		    	   			 messageKeys.put("organization_name", appointment.getProviderName());
		    	   			 messageKeys.put("location_name", appointment.getLocationName());
		    	   			 
		    	   	        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		    	   			String timeInAMPM = dateFormat.format(appointment.getStartDate());
		    	   			String timeInhr = timeInAMPM.substring(0, 2);
		    	   			String ampm = timeInAMPM.substring(6);
		    	   			String appointmentTime = timeInhr+":00 "+ampm;
		    	   			 messageKeys.put("date", sdfOnlyDateWithOutTime.format(appointment.getStartDate()));
		    	   			 messageKeys.put("time",appointmentTime);
		    	   			 messageKeys.put("unique_identifier", appointment.getAppointmentId().toString());
		    	   			 messageKeys.put("patient_user_name", patient.getPatientUserName());
		    	   			 messageKeys.put("patient_phone_number", patient.getPatientPhone());
		    	   			 messageKeys.put("patient_email", patient.getPatientEmail());
		    	   			
		    	   			 sendNotificationRequest.setMessageKeys(messageKeys);
		    	   			 sendToNotificationService(sendNotificationRequest);
		    	   			 
		    	   		  } // end for each recepient
		    	    		log.info("Time taken to send Appointmentr\n"
		    	    				+ " notifications for "+appointmentsList.size()+" Appointments is"+DateTimeUtility.calculateTimeDuration(start)+" seconds");
		} // end if appointmentList size  > 0
		
		
		
	    	    		
		return; // List<Map<String,String>; all recepient details the mesg to be sent
	}
	        
public void processPractitionerAppointmentSchedule(String messageTriggerId){
		
		
		String currentDayAppointmentsQuery = " select apt.id as appointmentId,apt.start as appointmentStart,"
				+ " apt.end as appointmentEnd ,"
				+ " apt.description as reasonToVisit ,"
				+ " ((apt.participant)[1].actor).reference as patient, " 
				+ "	((apt.participant)[3].actor).reference  as practitioner , "
				+ " ((apt.participant)[4].actor).display  as provider ," 
				+ "	((apt.participant)[5].actor).display  as location " 
				+ "	from project_asclepius.appointment apt " 
				+ "	WHERE apt.start >= CURRENT_DATE" 
				+ "	AND apt.start < (CURRENT_DATE + '1 day'::interval)"; 
		
		log.debug("Executing Process Appointment schedule Query: "+currentDayAppointmentsQuery+"");
		
		ArrayList<AppointmentInfo> appointmentsList = (ArrayList<AppointmentInfo>) jdbcTemplate.query(currentDayAppointmentsQuery,appointmentInofResultSetExtractor);
		Set<UUID> patientIds = new HashSet<UUID>();
		Set<UUID> practitionerIds  = new HashSet<UUID>();
		Date startTime = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(backendServiceDefaultDateFormat);
    	//we will use default backend timezone to check for appointments
    	Date currentDate = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone)).getTime();
	    String currentDateAsString = sdf.format(currentDate);
	    
	    SimpleDateFormat sdfOnlyDateWithOutTime = new SimpleDateFormat(backendServiceDefaultOnlyDateFormat);
    	//we will use default backend timezone to check for appointments
    	Date currentDateWithOutTime = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone)).getTime();
	    String currentDateWithOutTimeAsString = sdfOnlyDateWithOutTime.format(currentDateWithOutTime);
	    
	    
		if(appointmentsList == null || appointmentsList.isEmpty()) {
			log.info("No Appointments found for the date:" +currentDateWithOutTimeAsString+"\r\n");
			log.info("Exiting the method ProcessPractitionerAppointmentSchedule()");
			return;
		}else {
			log.info("Retrieved "+appointmentsList.size()+" Appointments for the date:"+currentDateWithOutTimeAsString+"");
		}
		for(AppointmentInfo appointment : appointmentsList) {
			patientIds.add(appointment.getPatientId())	;
			practitionerIds.add(appointment.getPractitionerId());
		}
		if(appointmentsList.size() > 0) {
			int i=1;
			String patientIdsInClause="";
			String patientInfoQuery = " select pat.id as patientId,usertab.username as userName, "
					+ " (pat.name)[1].given as firstName, "
					+ " (pat.name)[1].family as lastName, "
					+ " (pat.telecom)[2].value as email, "
					+ " (pat.telecom)[1].value as phoneNumber "
					+ "  from project_asclepius.patient pat, "
					+ " project_asclepius.auth_user usertab "
					+ " where pat.userid=usertab.id "
					+ "	 and pat.id in ("; 
			
		    		for(UUID id : patientIds) {
		    			
		    			patientIdsInClause+="'"+id+"'";
		    			
		    			if(i<patientIds.size()) {
		    				patientIdsInClause+=",";
		    				
		    			}
		    			i++;
		    			
		    			
		    		}
		    		patientInfoQuery+=patientIdsInClause+")";
		    		log.debug("Executing query to retrieve Patients information for Appointments: "+patientInfoQuery+"");
		    		HashMap<UUID,PatientInfo> patientInfos = (HashMap<UUID,PatientInfo>) jdbcTemplate.query(patientInfoQuery,patientsInfoResultSetExtractor);
		    		if(patientInfos == null || patientInfos.isEmpty()) {
		    			log.info("Not able to retrieve Patients with Appointments");
		    			log.info("Exiting the method ProcessPractitionerAppointmentSchedule()");
		    		}else {
		    			log.info("Retrieved "+patientInfos.size()+" Patients with Appointments for the date:"+currentDateWithOutTimeAsString+"");
		    		}
		    		
		    		i=1;
		    		String practitionerIdsInClause="";
		    		String practitionerInfoQuery = " select pract.id as practitionerId,usertab.username as userName, "
		    				+ " (pract.name)[1].given as firstName, "
		    				+ " (pract.name)[1].family as lastName, "
		    				+ " (pract.telecom)[2].value as email, "
		    				+ " (pract.telecom)[1].value as phoneNumber "
		    				+ "  from project_asclepius.practitioner pract, "
		    				+ " project_asclepius.auth_user usertab "
		    				+ " where pract.userid=usertab.id "
		    				+ "	 and pract.id in ("; 
		    		
		    	    		for(UUID id : practitionerIds) {
		    	    			
		    	    			practitionerIdsInClause+="'"+id+"'";
		    	    			
		    	    			if(i<practitionerIds.size()) {
		    	    				practitionerIdsInClause+=",";
		    	    				
		    	    			}
		    	    			i++;
		    	    			
		    	    			
		    	    		}
		    	    		practitionerInfoQuery+=practitionerIdsInClause+")";
		    	    		
		    	    		
		    	    		log.debug("Executing query to retrieve Practitioners information for Appointments: "+practitionerInfoQuery+"");
		    	    		HashMap<UUID,PractitionerInfo> practitionerInfos = (HashMap<UUID,PractitionerInfo>) jdbcTemplate.query(practitionerInfoQuery,practitionersInfoResultSetExtractor);
		    	    		
		    	    		if(practitionerInfos == null || practitionerInfos.isEmpty()) {
				    			log.info("Not able to retrieve Practitioners with Appointments");
				    			log.info("Exiting the method ProcessPractitionerAppointmentSchedule()");
				    		}else {
				    			log.info("Retrieved "+practitionerInfos.size()+" Practitioners with Appointments for the date:"+currentDateWithOutTimeAsString+"");
				    		}
		    	    		log.info("Iterating the Practitioners list and constructing the message template");
		    	    		for(UUID practId : practitionerInfos.keySet()) {
		    	    			PractitionerInfo pracDetials= practitionerInfos.get(practId);
		    	    			// use language translation here
		    	    			String appointmentTableHTML = "<html><body>Dear Dr."+pracDetials.getPractitionerFirstName()+", here is your schedule for today ";
		    	    			appointmentTableHTML=appointmentTableHTML+"<br><table border=\"1\" ><tr><td>from</td><td>to</td><td>Patient name</td><td>reason to visit</td><td>provider</td></tr>";
		    	    			for(AppointmentInfo appointment : appointmentsList){
		    	    				if(appointment.getPractitionerId().equals(pracDetials.getPractitionerId())) {
		    	    					PatientInfo patient = patientInfos.get(appointment.getPatientId());
		    	    					appointmentTableHTML=appointmentTableHTML+"<tr>"
		    	    							+"<td>"+ sdf.format(appointment.getStartDate())+"</td>"
		    	    							+"<td>"+sdf.format(appointment.getEndDate())+"</td>"
		    	    							+"<td>"+patient.getPatientFirstName()+"</td>"
		    	    							+"<td>"+appointment.getReasonToVisit()+"</td>"
		    	    							+"<td>"+appointment.getProviderName()+"</td>"
		    	    							+"</tr>";
		    	    				}
		    	    			}
		    	    			appointmentTableHTML=appointmentTableHTML+"</table></body><html>";
		    	    			pracDetials.setAppointmentHTMLTable(appointmentTableHTML);
		    	    			log.debug("Appointment schedule message for Practitioner: "+practId+":"+appointmentTableHTML+" \r\n");
		    	    			
		    	   				 
		    	   			 }
		    	    		log.info("Calling the Notification Service for "+practitionerInfos.size()+" Appointments one by one");
		    	    		Date start = new Date();
		    	    		for(UUID practitionerID : practitionerInfos.keySet()) {
		    	    			
		    	    		 PractitionerInfo practitioner= practitionerInfos.get(practitionerID);
		    	   			 NotificationRequest sendNotificationRequest = new  NotificationRequest();
		    	   			 sendNotificationRequest.setMessageTriggerId(Integer.parseInt(messageTriggerId));
		    	   			 HashMap messageKeys = new HashMap<String,String>();
		    	   			
		    	   			 
		    	   			
		    	   			 messageKeys.put("practitioner_first_name", practitioner.getPractitionerFirstName());
		    	   			 
		    	   			 messageKeys.put("unique_identifier", "AppointmentSchedule-"+practitioner.getPractitionerUserName()+"-"+currentDateWithOutTimeAsString);
		    	   			 
		    	   			 messageKeys.put("practitioner_user_name", practitioner.getPractitionerUserName());
		    	   			 messageKeys.put("practitioner_phone_number", practitioner.getPractitionerPhone());
		    	   			 messageKeys.put("practitioner_email", practitioner.getPractitionerEmail());
		    	   			 messageKeys.put("appointment_table", practitioner.getAppointmentHTMLTable());
		    	   			 sendNotificationRequest.setMessageKeys(messageKeys);
		    	   			 sendToNotificationService(sendNotificationRequest);
		    	   			 
		    	   		  } // end for each recepient
		    	    		log.info("Time taken to send Appointmentr\n"
		    	    				+ " schedule notifications for "+practitionerInfos.size()+"Practitioners is"+DateTimeUtility.calculateTimeDuration(start)+" seconds");
		} // end if appointmentList size  > 0
		
		
		
	    	    		
		return; // List<Map<String,String>; all recepient details the mesg to be sent
	}
	public ArrayList<String> processAppointmentSubsequentReminder(String messageTriggerId){
		
		return null; // pending implementation as this job is put on hold
		
		
	}
	
	public void processMedicalServiceRequestReportUploadReminder(String messageTriggerId){
		
		//Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Calendar startDate = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone));
		
		
		startDate.setTime(atStartOfDay(startDate.getTime()));
		
		startDate.add(Calendar.DAY_OF_MONTH, -3); // we will consider only medical service requests older than 3 days 
	    Timestamp startTime = Timestamp.of(startDate.getTime());
	    
	    Calendar endDate = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone));
	    
	    Timestamp endTime = Timestamp.of(atEndOfDay(endDate.getTime()));
	  
	    try {
	    	//System.out.println(sd.format(sd.parse(startDate.toString())));
	    	log.info("Retrieving the Medical Service Requests created between "+startTime.toString()+ " and "+endTime.toString()+"");
	        Date now = new Date();
	        Query<Entity> query = Query.newEntityQueryBuilder()
	                .setKind("ServiceRequest")
	                .setFilter(CompositeFilter.and(
	                   // PropertyFilter.gt("created_on", startDate), PropertyFilter.lt("created_on", endDate),PropertyFilter.isNull("attachments")))
	                		 PropertyFilter.gt("created_on", startTime), PropertyFilter.lt("created_on", endTime)))
	                .build();
	        QueryResults<Entity> serviceRequests = firestoreService.runQuery(query);
	        log.info("Time taken to retrieve Medical Service Requests is"+DateTimeUtility.calculateTimeDuration(now)+" seconds");
	       
	        ArrayList<ServiceRequestInfo> serviceRequestInfos = new ArrayList<ServiceRequestInfo>();
	        
	        if(serviceRequests.hasNext()) {
				log.info("Found some Medical Service requests which are 3 days older or earlier");
				
			}else{
				log.info("No Medical Service Requests are found which are 3 days older or earlier");
				log.info("Exiting!");
				return;
			}
	        
	        while (serviceRequests.hasNext()){
	        	 Entity serviceRequest = serviceRequests.next();
	        	 
	        	 if(serviceRequest.getList("attachments") == null || serviceRequest.getList("attachments").isEmpty()) {
	        		 // Construct the map
	        		 
	        		 log.info("Service Request: "+serviceRequest.getString("service_request_number")+" does not have attachments uploaded! We will consider this for sending the reminder");
	        		 
	        		 ServiceRequestInfo serviceRequestInfo = new ServiceRequestInfo();
	        		
	        		 serviceRequestInfo.setServiceRequestNumber(serviceRequest.getString("service_request_number"));
	        		 String requestType="request_type";
	        		 List requestTypes = (List) serviceRequest.getList("category.coding.display");
	        		 if(requestTypes != null && requestTypes.size() > 0) {
	        			 requestType = ((StringValue) requestTypes.get(0)).get();
	        		 }
	        		 serviceRequestInfo.setServiceRequestType(requestType);
		        	 String patientInfo = serviceRequest.getString("subject.reference");
		        	 if(patientInfo != null && patientInfo.contains("patient/")) {
		        		 patientInfo = patientInfo.substring(patientInfo.indexOf("/")+1);
		        		 serviceRequestInfo.setPatientId(UUID.fromString(patientInfo));
		        	 }else {
		        		 
		        		 log.debug("Medical Service Record does not have valid patient information format!\r\n"
			        		 		+ "Expecting patient/<<UUID>> format\r\n"
			        		 		+ " Service Request number: "+serviceRequestInfo.getServiceRequestNumber()+"");
			        		 log.warn("Skipping Medical Service Record: "+serviceRequestInfo.getServiceRequestNumber()+" as it does not have valid patient information");
			        		 serviceRequestInfo=null;
			        		 continue;
		        	 }
		        	 String practitionerInfo =serviceRequest.getString("requester.reference");
		        	 if(practitionerInfo != null && practitionerInfo.contains("practitioner/")) {
		        		 practitionerInfo = practitionerInfo.substring(practitionerInfo.indexOf("/")+1);
		        		 serviceRequestInfo.setPractitionerId(UUID.fromString(practitionerInfo));
		        	 }else {
		        		 log.debug("Medical Service Record does not have valid practitioner information format!\r\n"
			        		 		+ "Expecting practitioner/<<UUID>> format\r\n"
			        		 		+ " Service Request number: "+serviceRequestInfo.getServiceRequestNumber()+"");
			        		 log.warn("Skipping Medical Service Record: "+serviceRequestInfo.getServiceRequestNumber()+" as it does not have valid practitioner information");
			        		 serviceRequestInfo=null;
			        		 continue;
		        	 }
		        	    		
		        	 // set the service request type here.
		        	
		        	
		        	 serviceRequestInfos.add(serviceRequestInfo);
	        		 
	        	 }
	        	 
	         }
	         
	        if(serviceRequestInfos == null || serviceRequestInfos.isEmpty()) {
				log.info("No Medical Service Reuqests are filtered for sending reports upload reminder!");
				log.info("Exiting the method processMedicalServiceRequestReportUploadReminder()");
				return;
			}else {
				log.info("Identified "+serviceRequestInfos.size()+" Service Requests for sending reports upload reminder");
			}
	        
	 		Set<UUID> patientIds = new HashSet<UUID>();
	 		Set<UUID> practitionerIds  = new HashSet<UUID>();
	 		
	 		for(ServiceRequestInfo servicRequest : serviceRequestInfos) {
	 			patientIds.add(servicRequest.getPatientId())	;
	 			practitionerIds.add(servicRequest.getPractitionerId());
	 		}
	 		
	 		
	 		int i=1;
	 		String patientIdsInClause="";
	 		String patientInfoQuery = " select pat.id as patientId,usertab.username as userName, "
	 				+ " (pat.name)[1].given as firstName, "
	 				+ " (pat.name)[1].family as lastName, "
	 				+ " (pat.telecom)[2].value as email, "
	 				+ " (pat.telecom)[1].value as phoneNumber "
	 				+ "  from project_asclepius.patient pat, "
	 				+ " project_asclepius.auth_user usertab "
	 				+ " where pat.userid=usertab.id "
	 				+ "	 and pat.id in ("; 
	 		
	 	    		for(UUID id : patientIds) {
	 	    			
	 	    			patientIdsInClause+="'"+id+"'";
	 	    			
	 	    			if(i<patientIds.size()) {
	 	    				patientIdsInClause+=",";
	 	    				
	 	    			}
	 	    			i++;
	 	    			
	 	    			
	 	    		}
	 	    		patientInfoQuery+=patientIdsInClause+")";
	 	    		
	 	    		log.debug("Executing query to retrieve Patients information for Medical Service Requests: "+patientInfoQuery+"");
	 	    		
	 	    			 	    		
	 	    		HashMap<UUID,PatientInfo> patientInfos = (HashMap<UUID,PatientInfo>) jdbcTemplate.query(patientInfoQuery,patientsInfoResultSetExtractor);
	 	    		
	 	    		if(patientInfos == null || patientInfos.isEmpty()) {
	 	    			log.info("Not able to retrieve Patients with pending reports upload for Medical Service Requests");
	 	    			log.info("Exiting the method processMedicalServiceRequestReportUploadReminder()");
	 	    			return;
	 	    		}else {
	 	    			log.info("Retrieved "+patientInfos.size()+" Patients with pending reports upload to Medical Service Requests");
	 	    		}
	 	    		
	 	    		i=1;
	 	    		String practitionerIdsInClause="";
	 	    		String practitionerInfoQuery = " select pract.id as practitionerId,usertab.username as userName, "
	 	    				+ " (pract.name)[1].given as firstName, "
	 	    				+ " (pract.name)[1].family as lastName, "
	 	    				+ " (pract.telecom)[2].value as email, "
	 	    				+ " (pract.telecom)[1].value as phoneNumber "
	 	    				+ "  from project_asclepius.practitioner pract, "
	 	    				+ " project_asclepius.auth_user usertab "
	 	    				+ " where pract.userid=usertab.id "
	 	    				+ "	 and pract.id in ("; 
	 	    		
	 	    	    		for(UUID id : practitionerIds) {
	 	    	    			
	 	    	    			practitionerIdsInClause+="'"+id+"'";
	 	    	    			
	 	    	    			if(i<practitionerIds.size()) {
	 	    	    				practitionerIdsInClause+=",";
	 	    	    				
	 	    	    			}
	 	    	    			i++;
	 	    	    			
	 	    	    			
	 	    	    		}
	 	    	    		practitionerInfoQuery+=practitionerIdsInClause+")";
	 	    	    		
	 	    	    		log.debug("Executing query to retrieve Practitioners information for Medical Service Requests: "+practitionerInfoQuery+"");
	 	    	    		HashMap<UUID,PractitionerInfo> practitionerInfos = (HashMap<UUID,PractitionerInfo>) jdbcTemplate.query(practitionerInfoQuery,practitionersInfoResultSetExtractor);
	 	    	    		
	 	    	    		if(practitionerInfos == null || practitionerInfos.isEmpty()) {
	 		 	    			log.info("Not able to retrieve Practitioners with pending reports upload for Medical Service Requests");
	 		 	    			log.info("Exiting the method processMedicalServiceRequestReportUploadReminder()");
	 		 	    			return;
	 		 	    		}else {
	 		 	    			log.info("Retrieved "+practitionerInfos.size()+" Practitioners with pending reports upload to Medical Service Requests");
	 		 	    		}
	 	    	    		
	 	    	    		log.info("Calling the Notification Service for "+serviceRequestInfos.size()+" Medical Service Requests one by one");
		    	    		Date start = new Date();
	 	    	    		for(ServiceRequestInfo servicRequest : serviceRequestInfos) {
	 	    	    			 	    	    			
	 	    	   			 NotificationRequest sendNotificationRequest = new  NotificationRequest();
	 	    	   			 sendNotificationRequest.setMessageTriggerId(Integer.parseInt(messageTriggerId));
	 	    	   			 HashMap messageKeys = new HashMap<String,String>();
	 	    	   			 PatientInfo patient = patientInfos.get(servicRequest.getPatientId());
	 	    	   			 PractitionerInfo practitioner = practitionerInfos.get(servicRequest.getPractitionerId());
	 	    	   			
	 	    	   			 messageKeys.put("patient_first_name", patient.getPatientFirstName());
	 	    	   			 messageKeys.put("practitioner_name", practitioner.getPractitionerFirstName()+" "+practitioner.getPractitionerLastName());
	 	    	   			 messageKeys.put("request_type", servicRequest.getServiceRequestType());
	 	    	   			 messageKeys.put("service_request_number", servicRequest.getServiceRequestNumber());
	 	    	   			 messageKeys.put("patient_user_name", patient.getPatientUserName());
	 	    	   			 messageKeys.put("patient_phone_number", patient.getPatientPhone());
	 	    	   			 messageKeys.put("patient_email", patient.getPatientEmail());
	 	    	   			 messageKeys.put("unique_identifier", servicRequest.getServiceRequestNumber());
	 	    	   			 
	 	    	   			
	 	    	   			 sendNotificationRequest.setMessageKeys(messageKeys);
	 	    	   			 sendToNotificationService(sendNotificationRequest);
	 	    	   			 
	 	    	   		  } // end for each recepient
	 	    	    		log.info("Time taken to send reports upload reminder \n"
		    	    				+ " notifications for "+serviceRequestInfos.size()+" Medical Service Requests is"+DateTimeUtility.calculateTimeDuration(start)+" seconds");
	         
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	    
	   
	    
	    
		return; // List<Map<String,String>; all recepient details the mesg to be sent
	}
	
	
public void deleteOlderMessageDeliveries(String messageTriggerId){
	
	    boolean messageDeliveriesExist=true;
	    
	    while(messageDeliveriesExist) {
	    	
	    	//Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			Calendar startDate = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone));
			
			
			startDate.setTime(atStartOfDay(startDate.getTime()));
			int numberOfDaysOld = Integer.parseInt(backendServiceDefaultMessageDeleteDays);
			
			startDate.add(Calendar.DAY_OF_MONTH, -numberOfDaysOld); // we will consider only medical service requests older than 3 days 
		    Timestamp startTime = Timestamp.of(startDate.getTime());
		    
		   
		  
		    try {
		    	//System.out.println(sd.format(sd.parse(startDate.toString())));
		    	log.info("Retrieving the Message Deliveries which are older than "+numberOfDaysOld+" days "+startTime.toString());
		        Date now = new Date();
		        Query<Key> query = Query.newKeyQueryBuilder()
		                .setKind("Message_Deliveries")
		                .setFilter(
		                		 PropertyFilter.lt("Previous_Sent_Date", startTime))
		                .build();
		        QueryResults<Key> messageDeliveryKeys = firestoreService.getKeys(query);
		        log.info("Time taken to retrieve Message_Deliveries is"+DateTimeUtility.calculateTimeDuration(now)+" seconds");
		       
		        
		        
		        if(messageDeliveryKeys.hasNext()) {
					log.info("Found some Message Deliveries which are "+numberOfDaysOld+" days older");
					messageDeliveriesExist=true;
				    ArrayList<Key> keys = new ArrayList<Key>();
			        
			        
				    while (messageDeliveryKeys.hasNext()){
				        	Key key=messageDeliveryKeys.next();
				        	keys.add(key);
				     }
				    log.debug(keys.size()+" Message Deliveries with following Keys will be deleted\r\n"+keys.toString()+"");
				     firestoreService.deleteAll(keys);
					
				}else{
					log.info("No Message Deliveries exist whic are "+numberOfDaysOld+" days older");
					
					messageDeliveriesExist=false;
					
				}
		       
		       
			     }catch(Exception e) {
			    		e.printStackTrace();
			    		messageDeliveriesExist=false;
			     }
		       
		        
		    
	    }// end while messageDeliveriesExist
		    
		    
				return; // List<Map<String,String>; all recepient details the mesg to be sent
		       
		       
		        
	    }
		
		
	   
	
	
	
	
	
	
public static void main1(String args[]) {
	//Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
	//Calendar calendar = Calendar.getInstance(ZoneId.systemDefault().toInstant());
	
	calendar.setTime(atStartOfDay(new Date()));
	
	calendar.add(Calendar.DAY_OF_MONTH, -3);
    Timestamp startDate = Timestamp.of(calendar.getTime());
    Timestamp endDate = Timestamp.of(atStartOfDay(new Date()));
   // SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
    QueryResults<Entity> test = null;
    try {
    	//System.out.println(sd.format(sd.parse(startDate.toString())));
        System.out.println(startDate);
        System.out.println(endDate);
        
        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("ServiceRequest")
                .setFilter(CompositeFilter.and(
                   // PropertyFilter.gt("created_on", startDate), PropertyFilter.lt("created_on", endDate),PropertyFilter.isNull("attachments")))
                		 PropertyFilter.gt("created_on", startDate), PropertyFilter.lt("created_on", endDate)))
                .build();
      
         QueryResults<Entity> serviceRequests = getDataStore().run(query);
         while (serviceRequests.hasNext()){
        	 Entity serviceRequest = serviceRequests.next();
        	 if(serviceRequest.getList("attachments") == null || serviceRequest.getList("attachments").isEmpty()) {
        		 // Construct the map
        		 log.debug(serviceRequest.getString("subject.reference"));
        		 log.debug(serviceRequest.getString("requester.reference"));
        		 log.debug(serviceRequest.getString("service_request_number"));
        	 }
        	 
         }
       
    }catch(Exception e) {
    	e.printStackTrace();
    }
    
    System.out.println("test");
    
    
}

public static void mainnotused(String args[]) {

     int msg1MaxTries = 3;
     int msg2MaxTries = 4;
     int msg1Freq = 24; // hours
     int msg2Freq = 48 ;//
     int msg1currentSends = 0;
     int msg2currentSends = 0;
     
	//Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
	//Calendar calendar = Calendar.getInstance(ZoneId.systemDefault().toInstant());
	
	 calendar.setTime(atStartOfDay(new Date()));
	 
	 Date msg1lastSentDate = calendar.getTime();
	 System.out.println("first instance of sending message 1: "+msg1lastSentDate+"");
	 msg1currentSends++; 
	 System.out.println("number of tries completed for message 1: "+msg1currentSends+"");
	 Date endDate = null;
	// msg1, logic for end date calculation

	for (int i=2;i <= msg1MaxTries;i++){
		 calendar.add(Calendar.HOUR_OF_DAY, msg1Freq);
		 msg1lastSentDate= calendar.getTime();
		 System.out.println("next instance of sending message 1: "+msg1lastSentDate+"");
		 msg1currentSends++;
		 System.out.println("number of tries completed for message 1: "+msg1currentSends+"");
	}
	Date msg1EndDate = calendar.getTime();
	 System.out.println("end date for sending message 1: "+msg1lastSentDate+"");
	 System.out.println("number of tries for sending message 1: "+msg1currentSends+"");
	 
	 // message 2
	 calendar.setTime(atStartOfDay(new Date()));
	 
	 Date msg2lastSentDate = calendar.getTime();
	 
	 msg2currentSends++;
	 System.out.println("first instance of sending message 2: "+msg2lastSentDate+"");
	 System.out.println("number of tries for sending message 2: "+msg2currentSends+"");
	
	// msg1, logic for end date calculation

	for (int j = 2;j <= msg2MaxTries;j++){
		 calendar.add(Calendar.HOUR_OF_DAY, msg2Freq);
		 msg2lastSentDate= calendar.getTime();
		 System.out.println("next instance of sending message 2: "+msg2lastSentDate+"");
		 msg2currentSends++;
		 System.out.println("number of tries completed for message 2: "+msg2currentSends+"");
	}
	
	 Date msg2EndDate = calendar.getTime();
	 System.out.println("end date for sending message 2: "+msg2lastSentDate+"");
	 System.out.println("number of tries completed for message 2: "+msg2currentSends+"");
	
System.out.println("test");


}
public static Datastore getDataStore() throws IOException {
	
	GoogleCredentials googleCredentials = GoogleCredentials
            .fromStream(new ClassPathResource("fiebase_svc_account.json").getInputStream());
	
	DatastoreOptions datastoreOptions = DatastoreOptions.getDefaultInstance().toBuilder()
		       .setProjectId("health-compass-302720")
		        //.setCredentials(GoogleCredentials.getApplicationDefault())
		         .setCredentials(googleCredentials)
		        .build();
	Datastore db = datastoreOptions.getService();
		
   
    return db;
}

public static Date atStartOfDay(Date date) {
    LocalDateTime localDateTime = dateToLocalDateTime(date);
    LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
    return localDateTimeToDate(startOfDay);
}

public static Date atEndOfDay(Date date) {
    LocalDateTime localDateTime = dateToLocalDateTime(date);
    LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
    return localDateTimeToDate(endOfDay);
}

private static LocalDateTime dateToLocalDateTime(Date date) {
    return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
}

private static Date localDateTimeToDate(LocalDateTime localDateTime) {
    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
}
	
	
	
	

}
