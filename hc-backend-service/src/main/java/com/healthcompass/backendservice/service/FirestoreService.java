package com.healthcompass.backendservice.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.QueryResults;
import com.google.cloud.datastore.StructuredQuery.CompositeFilter;
import com.google.cloud.datastore.StructuredQuery.Filter;
import com.google.cloud.datastore.StructuredQuery.PropertyFilter;
import com.healthcompass.backendservice.controller.BackendNotificationController;
import com.healthcompass.backendservice.util.DateTimeUtility;

import lombok.extern.slf4j.Slf4j;



@Service
@Slf4j
public class FirestoreService {

	 private final Datastore dataStore;
	 
	 public FirestoreService(Datastore dataStore) {
	        this.dataStore = dataStore;
	 }
	 
	 
	 


public Map<String,String> getAllMessagesDueThisHour(String currentTimeInAMPM)  {

			
   
		    // get all the Messages due at this time. Because of restrictions on google data store query api, we are not able
	       // to apply composite filters with OR. Normally the condition that should be used is trigger_time is equal to current time or Every Hour
	       //because of the above restriction, we are retrieving all message ytiggers and then explicitely checking the trigger_time to match current time
		    Query<Entity> query = Query.newEntityQueryBuilder()
		    	    .setKind("Message_Triggers")
		    	    .build();
		    Date startTime = new Date();
		    QueryResults<Entity> message_triggers = dataStore.run(query);
		    log.debug("Time taken to retrieve all Message Triggers:"+DateTimeUtility.calculateTimeDuration(startTime)+" seconds");
		    Map<String,String> methodMap = new HashMap<String,String>();
		   
		    while(message_triggers.hasNext()) {
		    	Entity message_trigger = message_triggers.next();
		    	String messageTriggerTime = (String) message_trigger.getValue("trigger_time").get();
		    	if(messageTriggerTime != null && !messageTriggerTime.isEmpty()) {
		    		if(messageTriggerTime.equalsIgnoreCase("every hour") || messageTriggerTime.equalsIgnoreCase(currentTimeInAMPM)) {
		    			// Yes, this is the message that is due currently
		    			String messageTriggerId = message_trigger.getKey().getName();
		    			String methodName = (String) message_trigger.getValue("method_name").get();
		    			methodMap.put(messageTriggerId,methodName);
		    			log.info("Method:"+methodName+" with Trigger Id:"+messageTriggerId+" is due to be scheduled at "+messageTriggerTime+"");
		    			
		    		}
		    	}
		    }
		    
		    return methodMap;
		    
		
		
	       
	    }

public QueryResults<Entity> runQuery(Query query){
	
	return dataStore.run(query);
}

public QueryResults<Key> getKeys(Query query){
	
	return dataStore.run(query);
}

public void deleteAll(ArrayList<Key> keys){
	
	
		
		String methodName = "delete";
		Method method = null;
		try {
			  Class[] argTypes = new Class[] { Key[].class };
			  method = dataStore.getClass().getMethod(methodName,argTypes);
			
			}  catch (NoSuchMethodException e) { e.printStackTrace(); }
		
		
			if(method != null) {
				try {
				 Key[] a = new Key[keys.size()];
				 method.setAccessible(true);
				 method.invoke(dataStore,new Object[] {keys.toArray(a)});
			
			 
			} catch (IllegalArgumentException e) { e.printStackTrace(); }
			  catch (IllegalAccessException e) { e.printStackTrace(); }
			  catch (InvocationTargetException e) { e.printStackTrace(); }
			}
     	
	
}

public Entity getEntity(String kind, String id) {
	
	  Key kindKey = dataStore.newKeyFactory()
			    .setKind(kind)
			    .newKey(id);
	  
	  return dataStore.get(kindKey);
}



}
