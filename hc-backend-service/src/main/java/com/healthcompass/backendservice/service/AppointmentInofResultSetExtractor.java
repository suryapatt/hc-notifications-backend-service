package com.healthcompass.backendservice.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.backendservice.controller.BackendNotificationController;
import com.healthcompass.backendservice.model.AppointmentInfo;

import lombok.extern.slf4j.Slf4j;



@Component
@Slf4j
public class AppointmentInofResultSetExtractor implements ResultSetExtractor{
	
	
	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    
	    	List<AppointmentInfo> appointmentInfos = new ArrayList<AppointmentInfo>();
	        
	         while (rs.next()) {
	        	 AppointmentInfo appointmentInfo = new AppointmentInfo();
	        	 appointmentInfo.setAppointmentId(rs.getInt("appointmentId"));
	        	 appointmentInfo.setStartDate(rs.getTimestamp("appointmentStart"));
	        	 appointmentInfo.setEndDate(rs.getTimestamp("appointmentEnd"));
	        	 String patientInfo = rs.getString("patient");
	        	 if(patientInfo != null && patientInfo.contains("patient/")) {
	        		 patientInfo = patientInfo.substring(patientInfo.indexOf("/")+1);
	        		 appointmentInfo.setPatientId(UUID.fromString(patientInfo));
	        	 }else {
	        		 log.debug("Appointment record does not have valid patient information format!\r\n"
	        		 		+ "Expecting patient/<<UUID>> format\r\n"
	        		 		+ " Appointment ID:"+appointmentInfo.getAppointmentId()+"");
	        		 log.warn("Skipping Appointment record:"+appointmentInfo.getAppointmentId()+" as it does not have valid patient information");
	        		 appointmentInfo=null;
	        		 continue;
	        	 }
	        	 String practitionerInfo = rs.getString("practitioner");
	        	 if(practitionerInfo != null && practitionerInfo.contains("practitioner/")) {
	        		 practitionerInfo = practitionerInfo.substring(practitionerInfo.indexOf("/")+1);
	        		 appointmentInfo.setPractitionerId(UUID.fromString(practitionerInfo));
	        	 }else {
	        		 log.debug("Appointment record does not have valid practitioner information format!\r\n"
		        		 		+ "Expecting practitioner/<<UUID>> format\r\n"
		        		 		+ " Appointment ID:"+appointmentInfo.getAppointmentId()+"");
		        		 log.warn("Skipping Appointment record:"+appointmentInfo.getAppointmentId()+" as it does not have valid practitioner information");
		        		 appointmentInfo=null;
		        		 continue;
		         }
	        	 appointmentInfo.setReasonToVisit(rs.getString("reasonToVisit"));   		
	        	 appointmentInfo.setProviderName(rs.getString("provider"));
	        	 appointmentInfo.setLocationName(rs.getString("location"));
	        	
	        	
	        	 appointmentInfos.add(appointmentInfo);
	        	 
	        	
	         
	        }
	         
	        return  appointmentInfos ;
	    }
	
}
