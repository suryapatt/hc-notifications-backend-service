package com.healthcompass.backendservice;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties
public class BackendServiceApplication {

	@Value("${service-account-file-name}")
	private String serviceAccountFileName;
	
	@Value("${project-id}")
	private String projectId;
	
	public static void main(String[] args) {
		SpringApplication.run(BackendServiceApplication.class, args);
	}
	
	@Bean
	Datastore dataStore() throws IOException {
		
		GoogleCredentials googleCredentials = GoogleCredentials
	            .fromStream(new ClassPathResource(serviceAccountFileName).getInputStream());
		
		DatastoreOptions datastoreOptions = DatastoreOptions.getDefaultInstance().toBuilder()
			       .setProjectId(projectId)
			       .setCredentials(googleCredentials)
			        .build();
		Datastore db = datastoreOptions.getService();
			
	   
	    return db;
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate(); 
	}

}
