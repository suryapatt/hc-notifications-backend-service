package com.healthcompass.backendservice.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.healthcompass.backendservice.service.NotificationsSchedulerService;

import lombok.extern.slf4j.Slf4j;






@BasePathAwareController
@Slf4j
public class BackendNotificationController {
	
    
    
    @Autowired 
    NotificationsSchedulerService scheduler;
    
    @Value("${backend-service-timezone}")
	private String backendServiceTimeZone;
    
    @Value("${backend-service-language}")
	private String backendServiceLanguage;
             
    @Value("${backend-service-default-date-format}")
   	private String backendServiceDefaultDateFormat;
    
    
    @RequestMapping(value = "/processNotifications", method = RequestMethod.GET, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String processNotifications() {
		
    	SimpleDateFormat sdf = new SimpleDateFormat(backendServiceDefaultDateFormat);
    	// We will use India Timezone for all date time comparsions and for triggering scheduled jobs
    	Date currentDate = Calendar.getInstance(TimeZone.getTimeZone(backendServiceTimeZone)).getTime();
	    String strDate = sdf.format(currentDate);
	    log.info("Started background notifications process :: " + strDate);
	      
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		String timeInAMPM = dateFormat.format(new Date());
		String timeInhr = timeInAMPM.substring(0, 2);
		String ampm = timeInAMPM.substring(6);
		String exactHourTime = timeInhr+":00 "+ampm;
		scheduler.processAllMessagesDueThisHour(exactHourTime);
        return "sucess";
	}
	
	
}
