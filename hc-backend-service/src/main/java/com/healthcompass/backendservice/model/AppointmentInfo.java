package com.healthcompass.backendservice.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;



import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
public class AppointmentInfo implements Serializable {
  
	private Integer appointmentId;  // user
  	private Date startDate;
    private Date endDate;
    private UUID patientId;
    private UUID practitionerId;
    private String reasonToVisit;
    private String providerName;
    private String locationName;
    
   
   
	
	
   
    public AppointmentInfo() {
		
	}

	
	
}
